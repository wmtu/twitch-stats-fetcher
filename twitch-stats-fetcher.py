# utility to fetch stats for a twitch channel

# import system libraries
import os, json, sys, getopt, configparser, ast
import urllib3

# main function
if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)

    # get twitch oauth tokens
    http = urllib3.PoolManager()
    r = http.request('POST', 'https://id.twitch.tv/oauth2/token', 
        fields={
            'client_id': config['TWITCH']['CLIENT_ID'], 
            'client_secret': config['TWITCH']['CLIENT_SECRET'],
            'grant_type': 'client_credentials'})
    status1 = r.status
    tokens = json.loads(r.data.decode('utf-8'))

    # get the user id for the specified user
    r = http.request('GET', 'https://api.twitch.tv/helix/users',
        fields={'login': config['TWITCH']['USER_ID']},
        headers={
            'client-id': config['TWITCH']['CLIENT_ID'],
            'Authorization': 'Bearer ' + tokens['access_token']})
    status2 = r.status
    user = json.loads(r.data.decode('utf-8'))

    # output user info to a json file
    with open(config['GENERAL']['user_file'], 'w') as out_file:
        json.dump(user, out_file)

    # do a request for the stream stats
    r = http.request('GET', 'https://api.twitch.tv/helix/streams',
        fields={'user_id': user['data'][0]['id']},
        headers={
            'client-id': config['TWITCH']['CLIENT_ID'],
            'Authorization': 'Bearer ' + tokens['access_token']})
    status3 = r.status
    stats = json.loads(r.data.decode('utf-8'))

    # output stats to a json file
    with open(config['GENERAL']['stats_file'], 'w') as out_file:
        json.dump(stats, out_file)

    sys.exit(0)